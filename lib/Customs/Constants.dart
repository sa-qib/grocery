import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFFF2E24 );
const kPrimaryColorLight = Color(0xFFF1E6FF);
const kPrimaryBackground = Color(0xFF4B4136);
const kPrimaryColor2 = Color(0xFFF2D5C2);
const BackgroundColor = Color(0xFF212121);
const TextColor = Color(0xFF93CE46);
const kPrimaryColorLight2 = Color(0xFF86AFDD);